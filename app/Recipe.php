<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $hidden = [
    	'created_at',
    	'updated_at',
        'pivot'
    ];

    public $fillable = [
    	'name',
    	'description'
    ];


    public function ingredients() 
    {
    	return $this->belongsToMany('App\Ingredient', 'ingredient_recipe', 'recipe_id', 'ingredient_id')->withPivot('amount')->withTimestamps();
    }
}
