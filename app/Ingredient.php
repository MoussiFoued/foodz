<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $hidden = [
    	'created_at',
    	'updated_at',
    	'pivot'
    ];

    public $fillable = [
    	'name',
    	'measure',
    	'supplier'
    ];

    protected $appends = ['amount'];

    public function getAmountAttribute()
    {
        return $this->pivot->amount ?? Null;
    }
}
