<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $hidden = [
    	'created_at',
    	'updated_at'
    ];

    public $fillable = [
    	'delivery_date'
    ];

    public $guarded = [
    	'order_date'
    ];

    public function recipes() 
    {
    	return $this->belongsToMany('App\Recipe', 'box_recipe', 'box_id', 'recipe_id')->withTimestamps();
    }
}
