<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Box extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_date' => $this->created_at,
            'delivery_date' => $this->delivery_date,
            'recipes' => $this->recipes
        ];
    }
}
