<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\AlphaDashSpace;

class StoreNewIngredientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', new AlphaDashSpace, 'unique:ingredients', 'max:255'],
            'measure' => 'required|in:ml,cup,cups,l,lb,lbs,oz,g,kg,tsp,tbsp,dash,pieces,slices,sheet',
            'supplier' => ['required', new AlphaDashSpace, 'max:255']
        ];
    }

    public function messages()
    {
        return [
            'measure.in' => 'The measure field accept current values (ml, cup, cups, l, lb, lbs, oz, g, kg, tsp, tbsp, dash, pieces, slices, sheet)',
        ];
    }
}
