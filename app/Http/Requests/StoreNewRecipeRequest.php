<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewRecipeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            /*
            |------------------------------------------------------------------------------------------------------
            | IMPORTANT NOTE 
            |------------------------------------------------------------------------------------------------------
            |
            | The reason why i didn't apply any validation checks for 'name' and 'description' attrs here:
            |
            | Is because I beleive that xss attacks are an output problem. 
            | There is no security risk if you store <script>alert('Hacking your website in 3...2...')</script> 
            | in your database - it is just text - it doesnt mean anything.
            | I encourage escaping all output, regardless where it came from.
            | Here is a good discussion with further points on why you should filter output, not input:
            | https://stackoverflow.com/questions/11253532/html-xss-escape-on-input-vs-output
            */
            'name' => 'required|string|unique:recipes|max:255',
            'description' => 'required|string',
            'ingredients' => 'required|array',
            'ingredients.*.id' => 'required|exists:ingredients,id',
            'ingredients.*.amount' => 'required|numeric|gt:0',
        ];
    }
}
