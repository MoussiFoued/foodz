<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\StoreNewIngredientRequest;
use App\Http\Resources\Ingredient as IngredientResource;
use App\Http\Resources\IngredientCollection;
use Carbon\Carbon;

use App\Ingredient;
use App\Box;

class IngredientController extends Controller
{
    
	/**
     * @OA\Post(
     *      path="/ingredients/add/new",
     *      operationId="storeIngredient",
     *      tags={"Ingredients"},
     *      summary="Add new ingredient",
     *      description="Returns ingredient data",
     *     
     *      @OA\Parameter(
     *          name="name",
     *          description="Ingredient Name (Unique)",
     *          required=true,
     *          in="query",
     *          example="Greek Cheese",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="measure",
     *          description="Ingredient Measure",
     *          required=true,
     *          in="query",
     *          example="g",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="supplier",
     *          description="Ingredient Supplier",
     *          required=true,
     *          in="query",
     *          example="Jon Snow",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *  
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Response containing validation errors",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function addNew(StoreNewIngredientRequest $request) 
    {
        $ingredient = Ingredient::create($request->validated());
        
        $success['data'] = new IngredientResource($ingredient);
        
        $success['message'] = "Ingredient Created Succefully.";

        return response()->json($success, 201);
	}

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Foodz APIs Demo Documentation Using OpenApi",
     *      @OA\Contact(
     *          email="moussifoued@outlook.fr"
     *      ),
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server"
     * )
     * @OA\Get(
     *     path="/ingredients",
     *     summary="Get list of ingredients",
     *     tags={"Ingredients"},
     *     description="Return list of ingredients paginated",
     *     @OA\Parameter(
     *          name="per_page",
     *          description="Items Per Page",
     *          required=false,
     *          in="query",
     *          example="15",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Resource Not Found",
     *          @OA\JsonContent()
     *     ),
     * )
     *
     * @OA\Tag(
     *     name="Ingredients",
     *     description="API Endpoints of Ingredients"
     * )
     */
    public function list(Request $request) 
    {
        $perPage = abs((int) $request->per_page) ?? 10;

        $ingredients = Ingredient::paginate($perPage);

        if($ingredients->isEmpty()) {
            return response()->json([
                'data' => [],
                'message' => 'No Ingredients Found'
            ], 404);
        }

    	return new IngredientCollection($ingredients);

    }

     /**
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server"
     * )
     * @OA\Get(
     *     path="/ingredients/required/to/be/ordered",
     *     summary="Get list of ingredients required to be ordered by the company",
     *     tags={"Ingredients"},
     *     description="Return List of ingredients must be ordered",
     *     @OA\Parameter(
     *          name="order_date",
     *          description="Order Date",
     *          required=true,
     *          in="query",
     *          example="2020-05-01",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Resource Not Found",
     *          @OA\JsonContent()
     *     ),
     * )
     */
    public function mustBeOrdered(OrderRequest $request)
    {
        $from = $request->order_date ?? now();
        $to = (new Carbon($from))->addDays(7);

        $boxes = Box::with('recipes.ingredients')
        ->whereBetween('order_date', [$from, $to])
        ->get();

        if($boxes->isEmpty()) {
            return response()->json([
                'data' => [],
                'message' => 'No Ordered Boxes Found - No Ingredients Required To Be Ordered'
            ], 404);
        }

        $ingredientsToBeOrdered = [];

        foreach ($boxes as $box) {
            
            foreach ($box->recipes as $recipe) {
                
                foreach ($recipe->ingredients as $ingredient) {
                    
                    if(isset($ingredientsToBeOrdered[$ingredient->id])) {

                        $ingredientsToBeOrdered[$ingredient->id]['amount'] += $ingredient->amount;

                    } else {

                        $ingredientsToBeOrdered[$ingredient->id] = [
                            'id' => $ingredient->id,
                            'name' => $ingredient->name,
                            'amount' => $ingredient->amount,
                            'measure' => $ingredient->measure
                        ];
                    }

                    
                }

            }
        }

        $success['data'] = array_values($ingredientsToBeOrdered);

        return response()->json($success);
    }
}
