<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\Box as BoxResource;

use App\Http\Requests\CreateNewBoxRequest;

use App\Box;

class BoxController extends Controller
{
     /**
     * @OA\Post(
     *      path="/boxes/create/new",
     *      operationId="createBox",
     *      tags={"Boxes"},
     *      summary="Create new box",
     *      description="Returns box data",
     *     
     *      @OA\Parameter(
     *          name="delivery_date",
     *          description="Delivery Date",
     *          required=true,
     *          in="query",
     *          example="2020-05-04",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="recipes",
     *          description="Recipes IDs",
     *          style="deepObject",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *               @OA\Items(
     *                   type = "int"
     *               )
     *          )
     *      ),
     *  
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Response containing validation errors",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function createNew(CreateNewBoxRequest $request) 
    {
        $validated = $request->validated();
        
        $box = Box::create([
        	'delivery_date' => $validated['delivery_date']
        ]);

        $box->recipes()->attach($validated['recipes']);

        $success['data'] = new BoxResource($box);
        $success['message'] = "Box Created Succefully.";

        return response()->json($success, 201);
	}
}
