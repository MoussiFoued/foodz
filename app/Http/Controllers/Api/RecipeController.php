<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewRecipeRequest;
use App\Http\Resources\RecipeCollection;
use App\Http\Resources\Recipe as RecipeResource;
use Illuminate\Http\Request;

use App\Recipe;

class RecipeController extends Controller
{
    /**
     * @OA\Post(
     *      path="/recipes/add/new",
     *      operationId="storeRecipe",
     *      tags={"Recipes"},
     *      summary="Add new recipe",
     *      description="Returns recipe data",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="French omelet"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string",
     *                     example="This French omelet recipe is a classic and versatile favorite. Fill with cheese and ham or change it up by adding leftover cooked vegetables."
     *                 ),
     *                 @OA\Property(property="ingredients",
     *                      type="object",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer", example="1"),
     *                          @OA\Property(property="amount", type="float", example="200"),
     *                      ),
     *                 )
     *             )
     *         )
     *     ),
     *  
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Response containing validation errors",
     *          @OA\JsonContent()
     *      )
     * )
     */
    public function addNew(StoreNewRecipeRequest $request) 
    {
        $validated = $request->validated();
        
        $recipe = Recipe::create([
        	'name' => $validated['name'],
        	'description' => $validated['description']
        ]);

        $ingredients = [];

        foreach ($validated['ingredients'] as $ingredient) {
        	$ingredients[$ingredient['id']] = ['amount' => $ingredient['amount']];
        }

        $recipe->ingredients()->attach($ingredients);

        $success['data'] = new RecipeResource($recipe);
        $success['message'] = "Recipe Created Succefully.";

        return response()->json($success, 201);
	}

    /**
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server"
     * )
     * @OA\Get(
     *     path="/recipes",
     *     summary="Get list of recipes",
     *     tags={"Recipes"},
     *     description="Return list of recipes paginated",
     *     @OA\Parameter(
     *          name="per_page",
     *          description="Items Per Page",
     *          required=false,
     *          in="query",
     *          example="10",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Resource Not Found",
     *          @OA\JsonContent()
     *     ),
     * )
     */
    public function list(Request $request) 
    {
    	$perPage = abs((int) $request->per_page) ?? 10;

        $recipes = Recipe::with('ingredients')->paginate($perPage);

        if($recipes->isEmpty()) {
            return response()->json([
                'data' => [],
                'message' => 'No Recipes Found'
            ], 404);
        }

    	return new RecipeCollection($recipes);
    }
}
