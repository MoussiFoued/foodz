<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AlphaParagraph implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^[\pL\s\-\'\"\?\!\(\)\*\_\d+\.\,\:\[\]\{\}\;\@~#$%&+=`|\]+$/u', $value); 
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute may only contain alpha-numeric characters, spaces, commas, dots question marks, exclamation marks, parentheses, colons, semi-colons, Asterisks, Square Brackets, Braces, @ sign as well as dashes and underscores...';
    }
}
