<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::namespace('Api')->group(function () {
    Route::get('ingredients', 'IngredientController@list');
    Route::post('ingredients/add/new', 'IngredientController@addNew');

	Route::get('recipes', 'RecipeController@list');
    Route::post('recipes/add/new', 'RecipeController@addNew');

    Route::post('boxes/create/new', 'BoxController@createNew');  

    Route::get('ingredients/required/to/be/ordered ', 'IngredientController@mustBeOrdered');   

});

Route::fallback(function() {
	return response()->json(['message' => 'Route Not Found'], 404);
});


