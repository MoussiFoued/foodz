<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ingredient;
use Faker\Generator as Faker;

$factory->define(Ingredient::class, function (Faker $faker) {
    
    $availableMeasures = [
    	'ml', 
    	'cup', 
    	'cups', 
    	'l', 
    	'lb', 
    	'lbs', 
    	'oz', 
    	'g', 
    	'kg', 
    	'tsp', 
    	'tbsp', 
    	'dash', 
    	'pieces', 
        'slices',
    	'sheet'
    ];

    return [
        'name' => $faker->words(2, true),
        'measure' => $availableMeasures[array_rand($availableMeasures)],
        'supplier' => $faker->name
    ];
});
