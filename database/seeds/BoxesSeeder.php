<?php

use Illuminate\Database\Seeder;
use App\Box;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class BoxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('box_recipe')->truncate();
        DB::table('boxes')->truncate();

        $recipesIDs = [1, 2, 3, 4, 5];

        for ($i=0; $i <10 ; $i++) { 
        	
        	$box = Box::create([
        		'delivery_date' => Carbon::now()->addDays(rand(3,5))
        	]);

        	$randomRecipes =  Arr::random($recipesIDs, 4);

        	$box->recipes()->attach($randomRecipes);
        }

    }
}
