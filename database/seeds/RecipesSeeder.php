<?php

use Illuminate\Database\Seeder;
use App\Recipe;

class RecipesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('ingredient_recipe')->truncate();
        DB::table('recipes')->truncate();

        $data = json_decode(file_get_contents(database_path() . '/seeds/recipes.json'), true);

        foreach ($data as $item) {
            
            $recipe = Recipe::create([
                'name' => $item['name'],
                'description' => $item['description']
            ]);

            $recipe->ingredients()->attach($item['ingredients']);
        }
    }
}
