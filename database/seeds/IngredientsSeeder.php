<?php

use Illuminate\Database\Seeder;

class IngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredients')->truncate();

        $ingredients = json_decode(file_get_contents(database_path() . '/seeds/ingredients.json'), true);
        
        DB::table('ingredients')->insert($ingredients);
    }
}
