## Getting started
---
Download [Docker Desktop](https://www.docker.com/products/docker-desktop) for Mac or Windows. [Docker Compose](https://docs.docker.com/compose) will be automatically installed. On Linux, make sure you have the latest version of [Compose](https://docs.docker.com/compose/install/). 

## Installation Steps
----
**1. Clone the project off Bitbucket. You will need to have git installed to clone the project**
```bash
 git clone https://MoussiFoued@bitbucket.org/MoussiFoued/foodz.git
```
**2. Change directory to the cloned repository**
```bash
 cd foodz
```
**3. Make a copy of the laravel environment file**
```bash
 cp .env.example .env
```

**4. Set permissions correctly on folders which need to be writable**
```bash
 chmod -R a+w storage/app
 chmod -R a+w storage/framework
 chmod -R a+w storage/logs
 chmod -R a+w bootstrap/cache
 chmod -R a+w .env
```

**5. Run the following from your project root to build images and creating containers**
```bash
 chmod 755 ./start.sh
 ./start.sh
```
At this point you should be able to browse to
```bash
 http://localhost:8080/
```

## Testing
---
To run the application tests, you can run the following from your project root:

```bash
 composer test
```

This will run the feature tests that hits the database using the `sqlite` database connection.
## Note
---
Live Demo & Documentation should be available at http://localhost:8080/api/documentation

![Image of Swagger UI Interface](/foodz-demo-doc-sc.png)
