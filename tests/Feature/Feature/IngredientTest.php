<?php

namespace Tests\Feature\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Ingredient;
use IngredientsSeeder;
use DatabaseSeeder;


class IngredientTest extends TestCase
{
    use RefreshDatabase;
    
    public function testsRequiresNameMeasureAndSupplier()
    {
        $this->json('post', 'api/ingredients/add/new', [])
        ->assertStatus(422)
        ->assertJsonValidationErrors(['name','measure','supplier']);
    }

    public function testsRequiresUniqueName()
    {
        $this->seed(IngredientsSeeder::class);

        $payload = [
            'name' => 'basil',
            'measure' => 'kg',
            'supplier' => 'Foued'
        ];

        $this->json('post', 'api/ingredients/add/new', $payload)
        ->assertStatus(422)
        ->assertJson([
            'errors' => [
                'name' => [
                    'The name has already been taken.'  
                ]
            ]
        ]);
    }

    public function testsIngredientsAreCreatedCorrectly()
    {

        $payload = [
            'name' => 'Lorem',
            'measure' => 'g',
            'supplier' => 'Foued'
        ];

        $this->json('POST', 'api/ingredients/add/new', $payload)
        ->assertCreated()
        ->assertJsonStructure([
            'data' => [
                'id',
                'measure',
                'supplier'
            ],
        ]);
    }

    public function testIngredientsAreListedCorrectly()
    {
        factory(Ingredient::class)->create([
            'name' => 'Avocado',
            'measure' => 'pieces',
            'supplier' => 'Foued'
        ]);

        factory(Ingredient::class)->create([
            'name' => 'Bananas',
            'measure' => 'pieces',
            'supplier' => 'Foued'
        ]);

        $response = $this->json('GET', '/api/ingredients', [])
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => [
                '*' => ['name', 'measure', 'supplier']
            ],
            'links' => ['first', 'last', 'prev', 'next'],
            'meta' => ['current_page', 'per_page', 'total']
        ]);
    }

    public function testNoIngredientsRequiredToBeOrderedWhenNoBoxesFound()
    {

        $response = $this->json('GET', '/api/ingredients/required/to/be/ordered')
        ->assertStatus(404)
        ->assertJsonStructure([
            'data' => [
            ]
        ]);
    }

    public function testIngredientsRequiredToBeOrderedAreListedCorrectly()
    {
        $this->seed(DatabaseSeeder::class);

        $response = $this->json('GET', '/api/ingredients/required/to/be/ordered')
        ->assertStatus(200)
        ->assertJsonStructure([
            'data' => [
                '*' => ['name', 'measure', 'amount']
            ],
        ]);
    }
}
