<?php

namespace Tests\Feature\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Carbon\Carbon;

use App\Box;

use IngredientsSeeder;
use RecipesSeeder;

class BoxTest extends TestCase
{
    
    public function testsBoxesRequiresValidDeiveryDateToBeCreatedCorrectly()
    {
        $this->seed(IngredientsSeeder::class);
        $this->seed(RecipesSeeder::class);

        $payload = [
            'delivery_date' => now(),
            'recipes' => [
                1,
                2,
                3,
                4
            ]
        ];

        $response = $this->json('POST', 'api/boxes/create/new', $payload)
        ->assertStatus(422)
        ->assertJsonValidationErrors(['delivery_date']);;
    }

    public function testsOrderedBoxesRequiresFourMaximumRecipes()
    {
        $this->seed(IngredientsSeeder::class);
        $this->seed(RecipesSeeder::class);

        $payload = [
            'delivery_date' => Carbon::now()->addDays(3),
            'recipes' => [
                1,
                2,
                3,
                4,
                5
            ]
        ];

        $response = $this->json('POST', 'api/boxes/create/new', $payload)
        ->assertStatus(422)
        ->assertJsonValidationErrors(['recipes']);;
    }

    public function testsBoxesAreCreatedCorrectly()
    {
        $this->seed(IngredientsSeeder::class);
        $this->seed(RecipesSeeder::class);

        $payload = [
            'delivery_date' => Carbon::now()->addDays(3),
            'recipes' => [
                1,
                2,
                3,
                4
            ]
        ];

        $response = $this->json('POST', 'api/boxes/create/new', $payload)
        ->assertCreated()
        ->assertJsonStructure([
            'data' => [
                'id',
                'order_date',
                'delivery_date',
                'recipes'
            ],
        ]);
    }
}
