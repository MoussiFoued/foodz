<?php

namespace Tests\Feature\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Recipe;
use IngredientsSeeder;
use RecipesSeeder;

class RecipeTest extends TestCase
{
    use RefreshDatabase;

    public function testsRequiresIngredients()
    {
        $payload = [
            'name' => 'Pancake',
            'description' => 'A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter.'
        ];

        $this->json('post', 'api/recipes/add/new', [])
        ->assertStatus(422)
        ->assertJsonValidationErrors(['ingredients']);
    }

    public function testsRequiresUniqueName()
    {
        $this->seed(IngredientsSeeder::class);
        $this->seed(RecipesSeeder::class);

        $payload = [
            'name' => 'Baked Chicken Breast',
            'description' => 'Yummy Baked Chicken Breast'
        ];

        $this->json('post', 'api/recipes/add/new', $payload)
        ->assertStatus(422)
        ->assertJson([
            'errors' => [
                'name' => [
                    'The name has already been taken.'  
                ]
            ]
        ]);
    }



    public function testsRecipesAreCreatedCorrectly()
    {
        $this->seed(IngredientsSeeder::class);

        $payload = [
            'name' => 'Pancake',
            'description' => 'A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter.',
            'ingredients' => [
                ['id' => 1, 'amount' => 200],
                ['id' => 2, 'amount' => 300],
                ['id' => 3, 'amount' => 100]
            ]
        ];

        $response = $this->json('POST', 'api/recipes/add/new', $payload)
        ->assertCreated()
        ->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'description',
                'ingredients'
            ],
        ]);
    }

    public function testRecipesAreListedCorrectly()
    {
        $this->seed(IngredientsSeeder::class);
        $this->seed(RecipesSeeder::class);

        $response = $this->json('GET', '/api/recipes', [])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => ['id', 'name', 'description', 'ingredients']
                ],
                'links' => ['first', 'last', 'prev', 'next'],
                'meta' => ['current_page', 'per_page', 'total']
            ]);
    }
}
